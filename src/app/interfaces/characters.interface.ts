export interface ICharacterInterface {
  id: number,
  name: string,
  image: string,
}

export interface ICharacterResponseInterface {
  info: object,
  results: ICharacterInterface[],
}