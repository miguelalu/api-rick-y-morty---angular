import { Component, OnInit } from '@angular/core';

import { ServicioService } from './../services/servicio.service';

import { ICharacterInterface, ICharacterResponseInterface } from './../interfaces/characters.interface';


@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {
  characterList: object[] = [];

  constructor(private servicioService: ServicioService) { }

  ngOnInit() {
    this.servicioService.getCharacters()
    .subscribe((data: ICharacterResponseInterface) => {
      let results: ICharacterInterface[] = data.results;

      let formattedResults = results.map(({ id, name, image }) => ({
        id,
        name,
        image,
      }))

      this.characterList = [...formattedResults];
    })
   }
}
